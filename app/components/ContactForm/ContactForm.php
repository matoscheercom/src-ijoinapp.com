<?php

use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\Mail\Message;

/**
 * Contact Form component
 * @author Michal Bystricky <michal@fatchilli.com>
 * @author Martin Scheer <martin.scheer@fatchilli.com>
 */
class ContactForm extends Control {

    /** @var Mailer */
    private $mailer;

    /**
     * Mailer injector
     * @param Mailer $mailer
     */
    public function injectMailer(Mailer $mailer) {
        $this->mailer = $mailer;
    }

    /**
     * @return Form
     */
    public function createComponentContactForm() {
        $f = new Form();

        $tm = $this->presenter->context->getParameters();
        $lang = $tm['contact'][$this->presenter->lang]['form'];

        $f->addSubmit('submit', $lang['submit']);
        $f->addText('name', $lang['name'])
            ->setAttribute('placeholder', $lang['name'])
            ->setRequired();

        $f->addText('email', $lang['email'])
            ->setAttribute('placeholder', $lang['email'])
            ->addRule(Form::EMAIL, 'Nesprávne zadaná e-mailová adresa.')
            ->setRequired();

        $f->addText('phone', $lang['mobile'])
            ->setAttribute('placeholder', $lang['mobile']);

        $f->addTextArea('message', $lang['message'])
            ->setAttribute('placeholder', $lang['message'])
            ->setRequired();

        $f->addText('msg', 'Message');

        $f->onSuccess[] = $this->contactFormSubmitted;
        return $f;
    }

    /**
     * @param \Nette\Application\UI\Form $f
     */
    public function contactFormSubmitted(Form $f) {
        $v = $f->getValues();

        if (!trim($v->msg) && !stristr($v->message, 'url=')) {

            $params = $this->presenter->context->getParameters();
            $emailAddress = $params['mail']['contactForm'];

            $body = "Na stránke vila-merkur.com bol vyplnený kontaktný formulár.\n\n"
                    . "Meno a priezvisko: " . $v->name . "\n"
                    . "E-mail: " . $v->email . "\n"
                    . "Mobil: " . $v->phone . "\n"
                    . "Správa: \n\n" . $v->message;

            $message = new Message;
            $message
                ->setSubject("Kontaktný formulár")
                ->addTo($emailAddress)
                ->setBody($body);

            $this->mailer->sendMessage($message);
        }

        $this->template->emailSent = true;
        $this->redrawControl();
        $this->presenter->redrawControl();
    }

    public function render() {
        $tm = $this->presenter->context->getParameters();
        $this->template->contact = $tm['contact'][$this->presenter->lang];
        $this->template->lang = $this->presenter->lang;

        $this->template->setFile(__DIR__ . '/template.latte');
        $this->template->render();
    }

}