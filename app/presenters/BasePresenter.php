<?php

/**
 * Base presenter
 * @author Michal Bystricky <michal@fatchilli.com>
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter {

    /** @var Mailer */
    protected $mailer;

    /** @persistent */
    public $lang = 'en';

    /**
     * @param Mailer $mailer
     */
//    public function injectMailer(Mailer $mailer) {
//        $this->mailer = $mailer;
//    }



    /**
     * @return ContactForm
     */
    public function createComponentContactForm() {
        $c = new ContactForm;
        $c->injectMailer($this->mailer);
        return $c;
    }



    public function beforeRender() {
        parent::beforeRender();

        switch($this->lang) {
            case 'en':
//            case 'cs':
//            case 'sk':
//            case 'de':
                $this->template->lang = $this->lang;
                break;
            default:
                $this->redirect('this', array('lang' => 'en'));
                $this->terminate();
        }
        
        $tm = $this->context->getParameters();
/*
        $this->template->timeline = $tm['gallery'][$this->lang];
        $this->template->whatwedo = $tm['whatwedo'][$this->lang];
*/
        $this->template->head = $tm['head'][$this->lang];
//        $this->template->home = $tm['home'][$this->lang];
        $this->template->about = $tm['about'][$this->lang];
        $this->template->gallery = $tm['gallery'][$this->lang];
//        $this->template->bussiness = $tm['bussiness'][$this->lang];
//        $this->template->footprint = $tm['footprint'][$this->lang];
//        $this->template->visualization = $tm['visualization'][$this->lang];
//        $this->template->contact = $tm['contact'][$this->lang];
        $this->template->navigation = $tm['navigation'][$this->lang];
        $this->template->faq = $tm['faq'][$this->lang];
/*
        $this->template->menu = $tm['menu'][$this->lang];
 * 
 */
    }
}
