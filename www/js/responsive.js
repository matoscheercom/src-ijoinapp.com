/**
 * Created by martin on 18.10.2014.
 */
fullscreen();
jQuery(window).resize(fullscreen);

function fullscreen() {
    var masthead = jQuery('.masthead');
    var videoOffset = (jQuery('.js-header').height() + (jQuery('.js-steps').height()));
    var windowH = (jQuery(window).height() - videoOffset);
    var windowW = jQuery(window).width();

    masthead.width(windowW);
    masthead.height(windowH);
}
