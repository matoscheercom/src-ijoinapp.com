(function() {
    Faq = function() {

        var hederH = jQuery('.header').height();
        var navbarH = jQuery('.navbar').height();

        var scrollToIdElement = function() {
            var isAnimation = false;
            jQuery('.js-scroll').on('click', function(e) {
                e.preventDefault();
                if (isAnimation == false) {
                    isAnimation = true;
                    var linkId = jQuery(this).attr('href');
                    var elmentOffset = jQuery(linkId).offset().top - navbarH;
                    jQuery('html, body').animate({scrollTop: elmentOffset }, 700);
                    setTimeout(function() { isAnimation = false},700);
                }
            });
        };


        var bodyMarginTop = function() {
            var bodyH = hederH + navbarH;
            jQuery('body').css('margin-top', bodyH + 'px');
        };

        var setupFixedOnscroll = function() {
            if (jQuery(document).scrollTop() >= hederH) {
              jQuery('.navbar').addClass('fixed');
            } else {
              jQuery('.navbar').removeClass('fixed');
            }
        };

        return {
            init: function() {
                jQuery(document).ready(function() {
                    scrollToIdElement();
                    bodyMarginTop();
                });
                jQuery(window).scroll(function() {
                   setupFixedOnscroll();
                });
            }
        }
    }();

    Faq.init();

})();
