(function() {
    App = function() {

        var scroolToIdElement = function() {
            var isAnimation = false;
            jQuery('.js-scroll').on('click', function(e) {
                e.preventDefault();
                if (isAnimation == false) {
                    isAnimation = true;
                    var linkId = $(this).attr('href');
                    var elmentOffset = $(linkId).offset().top - jQuery('.js-header').height();
                    jQuery('html, body').animate({scrollTop: elmentOffset }, 700);
                    setTimeout(function() {isAnimation = false},700);
                    if (jQuery('.navbar-collapse.rollout')) {
                        jQuery('.navbar-collapse').removeClass('rollout');
                    }

                }
            });
        };

        var setupMasthead = function() {
            var masthead = jQuery('.masthead');
            var videoOffset = (jQuery('.js-header').height() + (jQuery('.js-steps').height()));
            var windowH = (jQuery(window).height() - videoOffset);
            var windowW = jQuery(window).width();

            masthead.width(windowW);
            masthead.height(windowH);
        };

        var setupToggleButton = function() {
            jQuery('.navbar-toggle').on('click', function(p) {
                p.preventDefault();
                var target = jQuery(this).data('target');
                if (jQuery(target).hasClass('rollout')) {
                    jQuery(target).removeClass('rollout');
                } else {
                    jQuery(target).addClass('rollout');
                }
            });

        };


        return {
            init: function() {
                jQuery(document).ready(function() {
                    setupMasthead();
                    scroolToIdElement();
                    setupToggleButton();
                });
            }
        }
    }();

    App.init();

})();
